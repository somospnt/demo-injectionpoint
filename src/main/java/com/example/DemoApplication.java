package com.example;

import java.util.logging.Logger;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class DemoApplication {

    @Bean
    @Scope("prototype")
    Logger logger(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }

    @Component
    public class ClaseB {

        private final Logger logger;

        public ClaseB(Logger logger) {
            this.logger = logger;
            logger.info("ClaseA!!!");
        }
    }

    @Component
    public class ClaseA {

        private final Logger logger;

        public ClaseA(Logger logger) {
            this.logger = logger;
            logger.info("ClaseB!!!");
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
